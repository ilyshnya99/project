﻿
function LineChart() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_dataviz);
        // Themes end
        var data = [];
         
     //   for (var i = 0; i < 50; i++) {
     //       var x1=x
      //      data.push(Number.parseFloat())
        // }
       
        var chart = am4core.create("chartdiv", am4charts.XYChart);//countr,research,marketing,sales
       var ind = 0.0;
      var n = parseInt( localStorage.getItem('level'));
        for (var i = 0; i < 51; i++) {

            data.push({ country: ind.toFixed(1), research: JSON.parse(localStorage.getItem(i.toString() + "f")), marketing: JSON.parse(localStorage.getItem(i.toString() + "s")), sales: JSON.parse(localStorage.getItem(i.toString() + "t")) });
            ind = ind + (n/51);
        }

        chart.data = data;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.title.text = "X , нм";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 50;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Волновая функция ";

        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "research";
        series.dataFields.categoryX = "country";
        series.name = "1 уровень";
        series.tooltipText = "1 уровень [bold]{valueY}[/]";
        series.strokeWidth = 3;

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "marketing";
        series2.dataFields.categoryX = "country";
        series2.name = "2 уровень";
        series2.tooltipText = "2 уровень [bold]{valueY}[/]";
        series2.strokeWidth = 3;
        series2.hidden = true

        var series3 = chart.series.push(new am4charts.LineSeries());
        series3.dataFields.valueY = "sales";
        series3.dataFields.categoryX = "country";
        series3.name = "3 уровень";
        series3.tooltipText = "3 уровень [bold]{valueY}[/]";
        series3.strokeWidth = 3;
        series3.hidden = true;


        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarX = new am4core.Scrollbar();
        // Add cursor
        chart.cursor = new am4charts.XYCursor();

        // Add legend
        chart.legend = new am4charts.Legend();
        
      //  chart.cursor = new am4charts.XYCursor();
        //chart.cursor.snapToSeries = series;
       // chart.cursor.xAxis = dateAxis;
       // chart.cursor = new am4charts.XYCursor();
      //  
         // Add cursor
     

        // Add legend
      //  chart.legend = new am4charts.Legend();

    }); // en
}

function LineChart1() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_dataviz);
        // Themes end
        var data = [];
       // x = JSON.parse(localStorage.getItem('key'));
        //   for (var i = 0; i < 50; i++) {
        //       var x1=x
        //      data.push(Number.parseFloat())
        // }

        var chart = am4core.create("chartdiv1", am4charts.XYChart);//countr,research,marketing,sales
        var ind = 0.0;
        var n = parseInt(localStorage.getItem('level'));
      
        for (var i = 0; i < 51; i++) {

          
            data.push({ country: ind, research: JSON.parse(localStorage.getItem(i.toString() + "c")), marketing: JSON.parse(localStorage.getItem(i.toString() + "p")), sales: JSON.parse(localStorage.getItem(i.toString() + "o")) });
            ind = ind + (n / 51);
        }

        chart.data = data;

        chart.data = data;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.title.text = "X , нм";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 50;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Значение плотности распределения";

        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "research";
        series.dataFields.categoryX = "country";
        series.name = "1 уровень";
        series.tooltipText = "1 уровень [bold]{valueY}[/]";
        series.strokeWidth = 3;

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "marketing";
        series2.dataFields.categoryX = "country";
        series2.name = "2 уровень";
        series2.tooltipText = "2 уровень [bold]{valueY}[/]";
        series2.strokeWidth = 3;
        series2.hidden = true

        var series3 = chart.series.push(new am4charts.LineSeries());
        series3.dataFields.valueY = "sales";
        series3.dataFields.categoryX = "country";
        series3.name = "3 уровень";
        series3.tooltipText = "3 уровень [bold]{valueY}[/]";
        series3.strokeWidth = 3;
        series3.hidden = true;


        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarX = new am4core.Scrollbar();
        // Add cursor
        chart.cursor = new am4charts.XYCursor();

        // Add legend
        chart.legend = new am4charts.Legend();

        //  chart.cursor = new am4charts.XYCursor();
        //chart.cursor.snapToSeries = series;
        // chart.cursor.xAxis = dateAxis;
        // chart.cursor = new am4charts.XYCursor();
        //  
        // Add cursor


        // Add legend
        //  chart.legend = new am4charts.Legend();

    }); // en
}

function LineChart2() {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv2", am4charts.XYChart3D);

        // Add data
        chart.data = [
            {
            "country": "1 ур.",
                "visits": 5.77825413979295E-21
        }, {
            "country": "2 ур.",
                "visits": 1.3001071814534135E-20
        }, {
            "country": "3 ур.",
                "visits": 2.31130165591718E-20
        }, {
            "country": "4ур.",
                "visits": 3.6114088373705929E-20
            },
            {
                "country": "5 ур.",
                "visits": 5.2004287258136541E-20
            }
        ];

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.labels.template.rotation = 0;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 0;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        valueAxis.dataFields.category = "visits";
    
        //categoryAxis.dataFields.category = "visits";
        valueAxis.title.text = "Энергия E ур.";
        valueAxis.title.fontWeight = "bold";
        valueAxis.renderer.labels.template.adapter.add("text", function (text) {
            return text
        });


        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.categoryY = "visits";
        series.dataFields.categoryX = "country";
        series.name = "Visits";
        series.tooltipText = "{categoryX}: [bold]{categoryY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function (fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", function (stroke, target) {
            return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

    });
}
